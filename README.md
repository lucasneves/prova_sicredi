Prova técnica de Automação de Teste - Sicredi Web

    • O script deve executar com sucesso seguindo os passos descritos para o teste
    • O script não pode ter problema de validações
    • A execução do script não pode gerar exceções (exceptions) durante a execução
    • Uso dos locators
    • Boas práticas de desenvolvimento aplicado ao script de teste
    
Desafio 1:

Passos:

    1. Acessar a página https://www.grocerycrud.com/demo/bootstrap_theme
    2. Mudar o valor da combo Select version para “Bootstrap V4 Theme”
    3. Clicar no botão Add Customer
    4. Preencher os campos do formulário com as informações
    5. Clicar no botão Sav
    6. Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção
    7. Fechar o browser

Desafio 2:

Pré-condição:
Execute todos os passos do Desafio 1

Passos:

    1. Clicar no link Go back to list
    2. Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi)
    3. Clicar no checkbox abaixo da palavra Actions
    4. Clicar no botão Delete
    5. Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada
    6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”
    7. Fechar o browser