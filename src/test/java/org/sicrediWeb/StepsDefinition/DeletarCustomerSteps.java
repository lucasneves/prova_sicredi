package org.sicrediWeb.StepsDefinition;

import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.sicrediWeb.Pages.DeletarCustomerPage;

public class DeletarCustomerSteps {

    DeletarCustomerPage deletarCustomerPage = new DeletarCustomerPage();

    @Quando("clico no Go Back")
    public void clicoNoGoBack() throws Exception {
        deletarCustomerPage.clicarBtnGoBack();
    }

    @Então("pesquiso pelo customer {string}")
    public void pesquisoPeloCustomer(String text) throws Exception {
        deletarCustomerPage.preencheSearchName(text);
    }

    @Então("clico no Delete")
    public void clicoNoDelete() throws Exception {
        deletarCustomerPage.marcarCheckBox();
        deletarCustomerPage.clicarBtnDelete();
        deletarCustomerPage.mensagemConfirmacaoDelete();
        deletarCustomerPage.clicarBtnDeletePopup();
    }

    @Então("devo visualizar uma mensagem de confirmação com a exclusão do customer")
    public void devoVisualizarUmaMensagemDeConfirmaçãoComAExlcusãoDoCustomer() throws Exception {
        deletarCustomerPage.validarMensagemExclusao();
    }
}
