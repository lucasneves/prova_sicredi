package org.sicrediWeb.StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import org.sicrediWeb.Pages.AdicionarCustomerPage;

public class AdicionarCustomerSteps {

    AdicionarCustomerPage adicionarCustomerPage = new AdicionarCustomerPage();

    @E("preencho com as informações do Customer:")
    public void preenchoComAsInformações(DataTable table) throws Exception {
        adicionarCustomerPage.preencherCampos(table);
        adicionarCustomerPage.clicarBtnSave();

    }

    @Então("devo visualizar uma mensagem de sucesso")
    public void devoVisualizarUmaMensagemDeSucesso() throws Exception {
        adicionarCustomerPage.validarMensagemSucesso();
    }

}
