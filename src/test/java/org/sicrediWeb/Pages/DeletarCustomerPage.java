package org.sicrediWeb.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

public class DeletarCustomerPage extends BasePage {

    public void clicarBtnGoBack() {
        btnClick(By.xpath("//button[@type=\"button\" and @id=\"save-and-go-back-button\"]"));
        waitForPageLoad();
        waitForElement(By.xpath("//button[@type=\"button\" and @class=\"close\" and @data-growl=\"dismiss\"]"));
        btnClick(By.xpath("//button[@type=\"button\" and @class=\"close\" and @data-growl=\"dismiss\"]"));
    }

    public void preencheSearchName(String text) {
        fieldInput(By.xpath("//input[@type=\"text\" and @name=\"customerName\"]"), text);
    }

    public void marcarCheckBox() {
        btnClick(By.xpath("//input[@class=\"select-all-none\" and @type=\"checkbox\"]"));
    }

    public void clicarBtnDelete() {
        btnClick(By.xpath("//*[@class=\"btn btn-outline-dark delete-selected-button\" and @title=\"Delete\"]"));
    }

    public void mensagemConfirmacaoDelete() {
        waitForClickableElement(By.xpath("//*[contains(text(),'Are you sure that you want to delete this 1 item?')]"));
        Assert.assertTrue(driver.getPageSource().contains("Are you sure that you want to delete this 1 item?"));
    }

    public void clicarBtnDeletePopup() {
        btnClick(By.xpath("//button[@type=\"button\" and @class=\"btn btn-danger delete-multiple-confirmation-button\"]"));
    }

    public void validarMensagemExclusao() {
        waitForElement(By.xpath("//*[@role='alert']//p"));
        String toasterMessage = getText(By.xpath("//*[@role='alert']//p"));
        Assert.assertEquals(toasterMessage, "Your data has been successfully deleted from the database.");
    }
}
