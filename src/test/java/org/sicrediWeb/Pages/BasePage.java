package org.sicrediWeb.Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sicrediWeb.Utils.Hooks;

public class BasePage {

    protected WebDriver driver;
    private final WebDriverWait wait;
    private static final int WAIT_TIMEMOUT = 30;

    public BasePage() {
        this.driver = Hooks.getDriver();
        this.wait = new WebDriverWait(driver, WAIT_TIMEMOUT);
    }

    public void waitForPageLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        this.wait.until(pageLoadCondition);
    }

    public void waitForElement(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForClickableElement(By by) {
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    /********* TextField e TextArea ************/

    public void fieldInput(By by, String text) {
        waitForClickableElement(by);
        driver.findElement(by).click();
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(text);
        driver.findElement(by).sendKeys(Keys.TAB);
    }

    public String getText(By by) {
        waitForElement(by);
        return driver.findElement(by).getText();
    }

    /********* Button ************/
    public void btnClick(By by) {
        waitForClickableElement(by);
        driver.findElement(by).click();
    }

    public String getValueElement(By by) {
        waitForElement(by);
        return driver.findElement(by).getAttribute("value");
    }

    /********* ComboBox ************/
    public void selectCombo(By by, String value) {
        waitForElement(by);
        WebElement element = driver.findElement(by);
        Select combo = new Select(element);
        combo.selectByValue(value);
    }
}
