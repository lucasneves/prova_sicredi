package org.sicrediWeb.Pages;

import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;

public class AdicionarCustomerPage extends BasePage {

    public void preencherCampos(DataTable table) {
        waitForPageLoad();

        List<Map<Object, Object>> data = table.asMaps(String.class, String.class);
        fieldInput(By.id("field-customerName"), (String) data.get(0).get("valor"));
        fieldInput(By.id("field-contactLastName"), (String) data.get(1).get("valor"));
        fieldInput(By.id("field-contactFirstName"), (String) data.get(2).get("valor"));
        fieldInput(By.id("field-phone"), (String) data.get(3).get("valor"));
        fieldInput(By.id("field-addressLine1"), (String) data.get(4).get("valor"));
        fieldInput(By.id("field-addressLine2"), (String) data.get(5).get("valor"));
        fieldInput(By.id("field-city"), (String) data.get(6).get("valor"));
        fieldInput(By.id("field-state"), (String) data.get(7).get("valor"));
        fieldInput(By.id("field-postalCode"), (String) data.get(8).get("valor"));
        fieldInput(By.id("field-country"), (String) data.get(9).get("valor"));
        btnClick(By.xpath("//*[text()='Select from Employeer']"));
        fieldInput(By.xpath("//*[@type='text' and @autocomplete='off']"), (String) data.get(10).get("valor"));
        fieldInput(By.id("field-creditLimit"), (String) data.get(11).get("valor"));

    }

    public void clicarBtnSave() {
        btnClick(By.xpath("//button[@type=\"submit\" and @id=\"form-button-save\"]"));
    }

    public void validarMensagemSucesso() {
        waitForElement(By.xpath("//*[text()='Your data has been successfully stored into the database. ']"));
        Assert.assertTrue(driver.getPageSource().contains("Your data has been successfully stored into the database."));
    }
}
