package org.sicrediWeb.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

public class HomePage extends BasePage {

    public void validarPaginaInicial() {
        // Validar que estamos na página correta
        waitForPageLoad();
        waitForElement(By.xpath("//*[contains(text(),\"Customers\")]"));
        String txt_Customers = getText(By.xpath("//*[contains(text(),\"Customers\")]"));
        Assert.assertEquals("Customers", txt_Customers);
    }

    public void mudarVersao(String text) {
        // Mudar versão da WebPage
        selectCombo(By.xpath("//*[@id='switch-version-select']"), text);
        waitForPageLoad();
        waitForElement(By.xpath("//*[contains(text(),\"Customers\")]"));
    }

    public void clicarBtnAddCustomer() {
        // Clicar Botõo Customer
        btnClick(By.xpath("//*[@class=\"el el-plus\"]"));
    }
}
