#language: pt
Funcionalidade: Deletar Customer existente

  Como usuário eu gostaria de deletar um customer já existente

  @TC02 @DeletarCustomer @SecondTest @SMOKE
  Cenário: Deletar Customer
    Quando clico no Go Back
    Então pesquiso pelo customer "Teste Sicredi"
    E clico no Delete
    Então devo visualizar uma mensagem de confirmação com a exclusão do customer
